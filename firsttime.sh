#!/bin/bash

folder=$1

rm -rf "./functions/${folder}/node_modules"
rm -rf "./functions/${folder}/ioContainer.js"
rm -rf "./functions/${folder}/service"
rm -rf "./functions/${folder}/model"
rm -rf "./functions/${folder}/.env"

cp -R node_modules "./functions/${folder}/node_modules"
cp ioContainer.js "./functions/${folder}/ioContainer.js"
cp -R service "./functions/${folder}/service"
cp -R model "./functions/${folder}/model"
cp .env "./functions/${folder}/.env"
cp package.json "./functions/${folder}/package.json"