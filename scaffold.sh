#!/bin/bash

folder=$1
mkdir "functions/${folder}";
echo '{"name": "test","description": "","runtime": "nodejs4.3","memory": 128,"timeout": 5,"role": "arn:aws:iam::954804258659:role/aws-nodejs-dev-IamRoleLambdaExecution-7LS6GPL2FJSJ"}' > "functions/$folder/function.json"
echo "'use strict'; var iocContainer = require('./ioContainer'); exports.handle = function(event, context, callback) { context.callbackWaitsForEmptyEventLoop = false; callback(null, event); }" > "functions/$folder/index.js"
./firsttime.sh $folder;

