var Promise = require('bluebird');
var MailGunEmailService = require('./MailGunEmailService');

function MailService(model) {
    this._mail = new MailGunEmailService();
    this._mailModel = model.emails;
    this._userModel = model.users;

    this.sentEmail = function (to, template, payload) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self._mailModel.find({
                where: {
                    slug: template
                }
            }).then(function (templateEmail) {
                self._mail.sentEmail(to, templateEmail.subject, templateEmail.html,
                    payload,
                    function (e, body) {
                        if (e) {
                            return reject(e);
                        } else {
                            console.log('Email Sent', body);
                            return resolve(body);
                        }
                    });
            })
        });
    };

    this.sentEmailToUser = function (user_id, template, payload) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self._userModel.find({
                where: {
                    id: user_id
                }
            }).then(function (user) {
                payload.first_name = user.first_name;
                payload.email = user.email;
                payload.last_name = user.last_name;
                payload.phone = user.phone;
                payload.profile_image = user.profile_image;
                payload.balance = user.balance;
                return self.sentEmail(user.email, template, payload);
            }).then(resolve).catch(reject);
        });
    };    
};

module.exports = MailService;