'use strict';
var iocContainer = require('./ioContainer');
var FirebaseService = require('./service/FirebaseService');
var SecurityService = require('./service/SecurityService');
var firebaseService = new FirebaseService();
var securityService = new SecurityService();

exports.handle = function (event, context, callback) {
    const userId = event.principalId;

    context.callbackWaitsForEmptyEventLoop = false;

    const where = {
        id: userId
    };

    iocContainer.models.users.find({ where: where }).then(
        function (user) {
            if (user) {
                return firebaseService.write('user', user.id, securityService.firebaseUserObject(user)).then(function (result) {
                    return callback(null, user);
                }).catch(function (err) {
                    console.log('FIREBASE GET PROFILE ERROR', err);
                    return callback(null, user);
                });
            } else {
                return callback(null, {
                    statusCode: 404,
                    message: 'Su Perfil no fue confirmado'
                });
            }
        }).catch(function (e) {
            console.log('Get GET ERROR', e);
            callback(null, {
                statusCode: 404,
                message: 'Su Perfil no fue confirmado'
            });
        });
}
