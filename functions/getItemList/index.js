'use strict';
var iocContainer = require('./ioContainer');
var PaginationService = require('./service/PaginationService');
exports.handle = function (event, context, callback) {
    context.callbackWaitsForEmptyEventLoop = false;
    const pagination = new PaginationService(event.query.page, event.query.numItems);

    let where = {
        $or: [{
            status: 'Active'
        }, {
            status: 'Flash'
        }]
    };
    let query = {
        attributes: [[iocContainer.db.fn('COUNT', iocContainer.db.col('id')), 'num']],
        order: [
            ['id', 'DESC']
        ],
        where: where
    };

    iocContainer.models.items.findAll(query).then(
        function (countResult) {
            if (countResult.length > 0) {
                pagination.setCount(countResult[0].dataValues.num);
                delete query.attributes;
                query.offset = pagination.getOffset();
                query.limit = pagination.getLimit();
                return iocContainer.models.items.findAll(query);
            } else {
                callback(null, {
                    statusCode: 200,
                    items: pagination.getItems(),
                    page: pagination.getPage(),
                    num_items: pagination.getCount(),
                    num_pages: pagination.getNumPages()
                });
            }
        }).then( function (items) {
            pagination.setItems(items);
            callback(null, {
                statusCode: 200,
                items: pagination.getItems(),
                page: pagination.getPage(),
                num_items: pagination.getCount(),
                num_pages: pagination.getNumPages()
            });
    }).catch( function (e) {
        console.log('Items GET ERROR', e);
        callback(null, {
            statusCode: 404,
            message: 'Items not found'
        });
    });
}
