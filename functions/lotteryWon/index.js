'use strict';
var iocContainer = require('./ioContainer');
var Promise = require('bluebird');
var lodash = require('lodash');
var MailService = require('./service/MailService');
var SNSService = require('./service/SNSService');
var mailService = new MailService(iocContainer.models);
/*
LotteryWon
1. Get Item
2. Change Item Status to Complete
3. Choose winner
4. Send winner email
5. Write to winner history
6. Get all item_user
7. Throw event for LotteryOne
 */
exports.handle = function (event, context, callback) {
    context.callbackWaitsForEmptyEventLoop = false;

    console.log(JSON.stringify(event));

    console.log('Lottery Won');

    if (event.Records && event.Records.length > 0) {
        const promiseList = event.Records.map(function (message) {
            let sns = '{}';
            if (message.Sns) {
                sns = message.Sns.Message;
            }
            const payload = JSON.parse(sns);
            const today = new Date();
            const todayStr = today.toISOString().slice(0, 10);

            console.log('Payload', payload);

            let currentWinner = null;
            let currentItem = null;
            const id = payload.id;

            if (!payload.id) {
                return null;
            }

            //1. Get Item
            return iocContainer.models.items.find({
                where: { id: id }
            }).then(function (item) {

                currentItem = lodash.clone(item);

                if (item.status == 'Active' &&
                    item.ticket_sold >= item.ticket_total) {
                    // 3. Choose winner
                    return iocContainer.db.query(
                        'select * from item_user WHERE item_id=' + id + ' ORDER BY RAND() limit 1');
                } else {
                    throw new Error('Item Not ready');
                }
            }).then(function (itemUser) {
                if (!itemUser) {
                    throw new Error('No One Register for Item');
                }
                console.log('WINNER OBJECT', itemUser);
                const winner = itemUser[0][0];
                return iocContainer.models.users.find({
                    where: { id: winner.user_id }
                });
            }).then(function (user) {
                currentWinner = lodash.clone(user);
                return currentItem.updateAttributes({
                    status: 'Complete',
                    winner_id: currentWinner.id
                });
            }).then(function (itemUser) {
                //4. Send winner email
                const snsService = new SNSService('sendEmail');
                return snsService.deliverMessage({
                    user_id: currentWinner.id,
                    template: 'lottery',
                    data: {
                        title: currentItem.title,
                        item_image: currentItem.image,
                        winner_code: currentItem.winner_code
                    }
                });

            }).then(function (mail) {
                const snsService = new SNSService('refreshItems');
                return snsService.deliverMessage({});
            }).then(function (mail) {
                //6. Get all item_user
                return iocContainer.models.item_user.findAll({
                    where: { item_id: id }
                });
            }).then(function (itemUsers) {
                const snsService = new SNSService('lotteryOne');
                return Promise.all(itemUsers.map(function (itemUser) {
                    return snsService.deliverMessage({
                        id: itemUser.user_id,
                        winner_id: currentWinner.id,
                        winner_first_name: currentWinner.first_name + ' ' + currentWinner.last_name,
                        title: currentItem.title,
                        item_image: currentItem.image
                    });
                }));
            }).then(function (messages) {
                return true;
            }).catch(function (error) {
                console.log('LOTTERY WON ERROR ID', id);
                console.log('LOTTERY WON ERROR', error);
                return null;
            });
        });

        Promise.all(promiseList).then(function (allLottery) {
            callback(null, allLottery);
        }).catch(function (err) {
            console.log('Refresh Room Error', err);
            callback(err);
        });
    }
}
