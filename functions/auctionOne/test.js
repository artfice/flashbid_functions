/*
AuctionOne
7. Write to notification for user
8. Write that they lost in history
9. Send Email
 */
var Promise = require('bluebird');
var index = require('./index');

var list = [
    new Promise(function (resolve, reject) {
        const event = {
            "Records": [
                {
                    "EventSource": "aws:sns",
                    "EventVersion": "1.0",
                    "EventSubscriptionArn": "arn:aws:sns:us-east-1:954804258659:auctionEnd:6b8a5421-f990-4cd9-9480-5cfc6e3956e5",
                    "Sns": {
                        "Type": "Notification",
                        "MessageId": "7d82b2c3-b8eb-508a-9a40-e201076ca80c",
                        "TopicArn": "arn:aws:sns:us-east-1:954804258659:auctionOne",
                        "Subject": null,
                        "Message": "{ \"item_id\":33, \"title\":\"Playstation\",\"item_image\":\"https://s3.amazonaws.com/images.flashbid/tile1-nt-sm.png\"}",
                        "Timestamp": "2016-12-08T01:54:15.396Z",
                        "SignatureVersion": "1",
                        "Signature": "hkUKfmk2J9jkGHlpP2r5vCPDuCTdP3BRTG7og2qAp9zKsQ03DqHqVLxpHicoZ0WovOPqsJHArPwgwR5lYXQkMHtMAQYUILq0qRTiuNckrAKK5V4X/xwZQJx67AeOz+lexFyus8FO4kB2mJ8YA/B2KWUhdmuz/xwaGhMgW5QxJKeAw0B3GBkYn06q4QhNb0hGl7lvcWfVzUq7WC6O7HKl8YHNX8GI/YOHIb4/94EYCkFM6nYzJUtuolGgqNKVmpy2CalHVZd+uCz44rN/WbnWxAQf2fLOEUWa7QPI8Zt4n2DsKR4tMST9E6grhteVjT6r9ezeOZhVeG/K1C2gjr2tXg==",
                        "SigningCertUrl": "https://sns.us-east-1.amazonaws.com/SimpleNotificationService-b95095beb82e8f6a046b3aafc7f4149a.pem",
                        "UnsubscribeUrl": "https://sns.us-east-1.amazonaws.com/?Action=Unsubscribe&SubscriptionArn=arn:aws:sns:us-east-1:954804258659:auctionEnd:6b8a5421-f990-4cd9-9480-5cfc6e3956e5",
                        "MessageAttributes": {}
                    }
                }
            ]
        };
        index.handle(event, {}, function (e, data) {
            resolve(data);
        })
    }),
    new Promise(function (resolve, reject) {
        const event = {
            "Records": [
                {
                    "EventSource": "aws:sns",
                    "EventVersion": "1.0",
                    "EventSubscriptionArn": "arn:aws:sns:us-east-1:954804258659:auctionEnd:6b8a5421-f990-4cd9-9480-5cfc6e3956e5",
                    "Sns": {
                        "Type": "Notification",
                        "MessageId": "7d82b2c3-b8eb-508a-9a40-e201076ca80c",
                        "TopicArn": "arn:aws:sns:us-east-1:954804258659:auctionOne",
                        "Subject": null,
                        "Message": "{\"id\":4, \"title\":\"Playstation\",\"item_image\":\"https://s3.amazonaws.com/images.flashbid/tile1-nt-sm.png\"}",
                        "Timestamp": "2016-12-08T01:54:15.396Z",
                        "SignatureVersion": "1",
                        "Signature": "hkUKfmk2J9jkGHlpP2r5vCPDuCTdP3BRTG7og2qAp9zKsQ03DqHqVLxpHicoZ0WovOPqsJHArPwgwR5lYXQkMHtMAQYUILq0qRTiuNckrAKK5V4X/xwZQJx67AeOz+lexFyus8FO4kB2mJ8YA/B2KWUhdmuz/xwaGhMgW5QxJKeAw0B3GBkYn06q4QhNb0hGl7lvcWfVzUq7WC6O7HKl8YHNX8GI/YOHIb4/94EYCkFM6nYzJUtuolGgqNKVmpy2CalHVZd+uCz44rN/WbnWxAQf2fLOEUWa7QPI8Zt4n2DsKR4tMST9E6grhteVjT6r9ezeOZhVeG/K1C2gjr2tXg==",
                        "SigningCertUrl": "https://sns.us-east-1.amazonaws.com/SimpleNotificationService-b95095beb82e8f6a046b3aafc7f4149a.pem",
                        "UnsubscribeUrl": "https://sns.us-east-1.amazonaws.com/?Action=Unsubscribe&SubscriptionArn=arn:aws:sns:us-east-1:954804258659:auctionEnd:6b8a5421-f990-4cd9-9480-5cfc6e3956e5",
                        "MessageAttributes": {}
                    }
                }
            ]
        };
        index.handle(event, {}, function (e, data) {
            resolve(data);
        })
    }),    
    new Promise(function (resolve, reject) {
        const event = {
            "Records": [
                {
                    "EventSource": "aws:sns",
                    "EventVersion": "1.0",
                    "EventSubscriptionArn": "arn:aws:sns:us-east-1:954804258659:auctionEnd:6b8a5421-f990-4cd9-9480-5cfc6e3956e5",
                    "Sns": {
                        "Type": "Notification",
                        "MessageId": "7d82b2c3-b8eb-508a-9a40-e201076ca80c",
                        "TopicArn": "arn:aws:sns:us-east-1:954804258659:auctionOne",
                        "Subject": null,
                        "Message": "{\"id\":4, \"item_id\":33,\"item_image\":\"https://s3.amazonaws.com/images.flashbid/tile1-nt-sm.png\"}",
                        "Timestamp": "2016-12-08T01:54:15.396Z",
                        "SignatureVersion": "1",
                        "Signature": "hkUKfmk2J9jkGHlpP2r5vCPDuCTdP3BRTG7og2qAp9zKsQ03DqHqVLxpHicoZ0WovOPqsJHArPwgwR5lYXQkMHtMAQYUILq0qRTiuNckrAKK5V4X/xwZQJx67AeOz+lexFyus8FO4kB2mJ8YA/B2KWUhdmuz/xwaGhMgW5QxJKeAw0B3GBkYn06q4QhNb0hGl7lvcWfVzUq7WC6O7HKl8YHNX8GI/YOHIb4/94EYCkFM6nYzJUtuolGgqNKVmpy2CalHVZd+uCz44rN/WbnWxAQf2fLOEUWa7QPI8Zt4n2DsKR4tMST9E6grhteVjT6r9ezeOZhVeG/K1C2gjr2tXg==",
                        "SigningCertUrl": "https://sns.us-east-1.amazonaws.com/SimpleNotificationService-b95095beb82e8f6a046b3aafc7f4149a.pem",
                        "UnsubscribeUrl": "https://sns.us-east-1.amazonaws.com/?Action=Unsubscribe&SubscriptionArn=arn:aws:sns:us-east-1:954804258659:auctionEnd:6b8a5421-f990-4cd9-9480-5cfc6e3956e5",
                        "MessageAttributes": {}
                    }
                }
            ]
        };
        index.handle(event, {}, function (e, data) {
            resolve(data);
        })
    }),        
    new Promise(function (resolve, reject) {
        const event = {
            "Records": [
                {
                    "EventSource": "aws:sns",
                    "EventVersion": "1.0",
                    "EventSubscriptionArn": "arn:aws:sns:us-east-1:954804258659:auctionEnd:6b8a5421-f990-4cd9-9480-5cfc6e3956e5",
                    "Sns": {
                        "Type": "Notification",
                        "MessageId": "7d82b2c3-b8eb-508a-9a40-e201076ca80c",
                        "TopicArn": "arn:aws:sns:us-east-1:954804258659:auctionOne",
                        "Subject": null,
                        "Message": "{\"id\":4, \"item_id\":33,\"title\":\"Playstation\"}",
                        "Timestamp": "2016-12-08T01:54:15.396Z",
                        "SignatureVersion": "1",
                        "Signature": "hkUKfmk2J9jkGHlpP2r5vCPDuCTdP3BRTG7og2qAp9zKsQ03DqHqVLxpHicoZ0WovOPqsJHArPwgwR5lYXQkMHtMAQYUILq0qRTiuNckrAKK5V4X/xwZQJx67AeOz+lexFyus8FO4kB2mJ8YA/B2KWUhdmuz/xwaGhMgW5QxJKeAw0B3GBkYn06q4QhNb0hGl7lvcWfVzUq7WC6O7HKl8YHNX8GI/YOHIb4/94EYCkFM6nYzJUtuolGgqNKVmpy2CalHVZd+uCz44rN/WbnWxAQf2fLOEUWa7QPI8Zt4n2DsKR4tMST9E6grhteVjT6r9ezeOZhVeG/K1C2gjr2tXg==",
                        "SigningCertUrl": "https://sns.us-east-1.amazonaws.com/SimpleNotificationService-b95095beb82e8f6a046b3aafc7f4149a.pem",
                        "UnsubscribeUrl": "https://sns.us-east-1.amazonaws.com/?Action=Unsubscribe&SubscriptionArn=arn:aws:sns:us-east-1:954804258659:auctionEnd:6b8a5421-f990-4cd9-9480-5cfc6e3956e5",
                        "MessageAttributes": {}
                    }
                }
            ]
        };
        index.handle(event, {}, function (e, data) {
            resolve(data);
        })
    }),            
    new Promise(function (resolve, reject) {
        const event = {
            "Records": [
                {
                    "EventSource": "aws:sns",
                    "EventVersion": "1.0",
                    "EventSubscriptionArn": "arn:aws:sns:us-east-1:954804258659:auctionEnd:6b8a5421-f990-4cd9-9480-5cfc6e3956e5",
                    "Sns": {
                        "Type": "Notification",
                        "MessageId": "7d82b2c3-b8eb-508a-9a40-e201076ca80c",
                        "TopicArn": "arn:aws:sns:us-east-1:954804258659:auctionOne",
                        "Subject": null,
                        "Message": "{\"id\":4, \"item_id\":33, \"title\":\"Playstation\",\"item_image\":\"https://s3.amazonaws.com/images.flashbid/tile1-nt-sm.png\"}",
                        "Timestamp": "2016-12-08T01:54:15.396Z",
                        "SignatureVersion": "1",
                        "Signature": "hkUKfmk2J9jkGHlpP2r5vCPDuCTdP3BRTG7og2qAp9zKsQ03DqHqVLxpHicoZ0WovOPqsJHArPwgwR5lYXQkMHtMAQYUILq0qRTiuNckrAKK5V4X/xwZQJx67AeOz+lexFyus8FO4kB2mJ8YA/B2KWUhdmuz/xwaGhMgW5QxJKeAw0B3GBkYn06q4QhNb0hGl7lvcWfVzUq7WC6O7HKl8YHNX8GI/YOHIb4/94EYCkFM6nYzJUtuolGgqNKVmpy2CalHVZd+uCz44rN/WbnWxAQf2fLOEUWa7QPI8Zt4n2DsKR4tMST9E6grhteVjT6r9ezeOZhVeG/K1C2gjr2tXg==",
                        "SigningCertUrl": "https://sns.us-east-1.amazonaws.com/SimpleNotificationService-b95095beb82e8f6a046b3aafc7f4149a.pem",
                        "UnsubscribeUrl": "https://sns.us-east-1.amazonaws.com/?Action=Unsubscribe&SubscriptionArn=arn:aws:sns:us-east-1:954804258659:auctionEnd:6b8a5421-f990-4cd9-9480-5cfc6e3956e5",
                        "MessageAttributes": {}
                    }
                }
            ]
        };
        index.handle(event, {}, function (e, data) {
            // console.log('Error', e);
            console.log('User Broke Answer', data);
            resolve(data);
        })
    })
];

Promise.all(list).then(function (results) {
    console.log(results);
});
