'use strict';
var iocContainer = require('./ioContainer');
var lodash = require('lodash');
var Sequelize = require('sequelize');
var FirebaseService = require('./service/FirebaseService');
var firebaseService = new FirebaseService();

exports.handle = function (event, context, callback) {
    const userId = event.principalId;

    context.callbackWaitsForEmptyEventLoop = false;
    const body = event.body;
    const code = body.code;
    let currentBidCard = null;
    let currentUser = null;
    let currentSystem = null;
    let invalidCard = null;
    let bidTimeOut = null;
    let newBalance = 0;
    const today = new Date();
    const todayStr = today.toISOString().slice(0, 10);

    iocContainer.db.transaction({
        isolationLevel: Sequelize.Transaction.ISOLATION_LEVELS.SERIALIZABLE
    }).then(function (t) {
        return iocContainer.models.bid_cards.find({
            where: {
                code: code
            }
        }, { transaction: t }).then(function (bidCard) {
            if(!bidCard || bidCard.status !== 0 || bidCard.user_id !== 0) {
                console.log('invalid condition');
                invalidCard = true;
                return bidCard;
            } else {
                return bidCard.updateAttributes({
                    user_id: userId,
                    status: 1,
                    update_at: new Date()
                }, { transaction: t });
            }
        }).then(function (bidCard) {
            currentBidCard = lodash.clone(bidCard);
            return iocContainer.models.users.find({
                where: {
                    id: userId
                }
            }, { transaction: t })
        }).then(function (user) {
            currentUser = lodash.clone(user);
            if (invalidCard) {
                if (currentUser.bid_attempts > 3) {
                    bidTimeOut = true;
                    throw new Error('You have guess Bid Card incorrectly too many times');
                }
                return user.updateAttributes({
                    bid_attempts: currentUser.bid_attempts + 1,
                    update_at: new Date()
                }, { transaction: t });
            } else {
                newBalance = currentUser.balance + currentBidCard.amount + currentBidCard.bonus;
                return user.updateAttributes({
                    balance: newBalance,
                    update_at: new Date()
                }, { transaction: t });
            }
        }).then(function (user) {
            user = user.toJSON();
            delete user.access_token;
            delete user.refresh_token;
            delete user.confirm_token;
            delete user.reset_token;
            delete user.term_condition;
            delete user.role;
            delete user.password;
            return firebaseService.write('user', user.id, user).then(function(result){
                return iocContainer.models.system.find({
                    where: {
                        id: 1
                    }
                }, { transaction: t });
            });
        }).then(function (system) {
            currentSystem = lodash.clone(system);
            if (!invalidCard) {
            return system.updateAttributes({
                num_bid_sold_card: currentSystem.num_bid_sold_card + 1,
                value_bid_sold_card: currentSystem.value_bid_sold_card + currentBidCard.amount,
                bonus_bid_sold_card: currentSystem.bonus_bid_sold_card + currentBidCard.bonus
            }, { transaction: t });
            } else {
                return system;
            }
        }).then(function (system) {
            if (!invalidCard) {
            return iocContainer.models.transactions.create({
                action: 'UserBoughtCard',
                user_id: userId,
                item_id: currentBidCard.id,
                amount: currentBidCard.amount + currentBidCard.bonus,
                userOldTotal: currentUser.balance - currentBidCard.amount - currentBidCard.bonus,
                userTotal: newBalance,
                systemOldTotal: currentSystem.earned,
                systemTotal: currentSystem.earned,
                notes: 'User ' + userId + ' Bought Bid Card ' + currentBidCard.code +
                ' from the App for amount ' + currentBidCard.amount + ' with bonus ' + currentBidCard.bonus,
                created_at: todayStr,
                updated_at: today
            }, { transaction: t });
            } else {
                return system;
            }
        }).then(function (result) {
            if (!invalidCard) {
            return iocContainer.models.history.create({
                user_id: userId,
                action: 'BoughtCard',
                message: 'Bought Bid Card ' + currentBidCard.amount + ' with bonus ' + currentBidCard.bonus,
                read: 0,
                created_at: todayStr,
                updated_at: Math.ceil(today.getTime() / 10000)
            }, { transaction: t });
            } else {
                return result;
            }
        }).then(function (result) {
            t.commit();
            if(!invalidCard) {
                return callback(null, {
                    statusCode: 200,
                    message: 'Bid Card Added'
                });
            } else {
                return callback(null, {
                        statusCode: 403,
                        message: 'Bid Card is invalid'
                    });   
            }

        }).catch(function (err) {
            console.log('Bid Card Purchase Error ', err);
            t.rollback();
            if (bidTimeOut) {
                return callback(null, {
                    statusCode: 403,
                    message: 'You have guessed the bid card wrong 4 times. Please go to vendor to unlock'
                });                
            } else {
                return callback(null, {
                    statusCode: 403,
                    message: 'Bid Card is invalid'
                });
            }
        });
    });
};