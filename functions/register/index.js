'use strict';
var iocContainer = require('./ioContainer');
var SecurityService = require('./service/SecurityService');
var PaginationService = require('./service/PaginationService');
var AuthenticationService = require('./service/AuthenticationService');
var MailService = require('./service/MailService');
var lodash = require('lodash');
var authenticationService = new AuthenticationService();
var mailService = new MailService(iocContainer.models);
var SNSService = require('./service/SNSService');
const securityService = new SecurityService();

exports.handle = function (event, context, callback) {
    context.callbackWaitsForEmptyEventLoop = false;

    const body = event.body;

    const first_name = body && body.first_name ? body.first_name : '';
    const last_name = body && body.last_name ? body.last_name : '';
    const email = body && body.email ? body.email : '';
    const password = body && body.password ? body.password : '';
    const device_id = body && body.device_id ? body.device_id : '';
    const phone_number = body && body.phone_number ? body.phone_number : 0;

    let currentUser = null;
    let accessToken = '';

    const snsService = new SNSService('refreshItems');

    const errors = {
        status: false
    };

    if (first_name.length < 1) {
        errors.status = true;
        errors.first_name = 'Nombre es requerido.';
    }
    if (last_name.length < 1) {
        errors.status = true;
        errors.last_name = 'Apellido es requerido';
    }
    if (email.length < 1) {
        errors.status = true;
        errors.email = 'Ingrese un correo electrónico Valido';
    }
    if (password.length < 8) {
        errors.status = true;
        errors.password = 'Contraseña es muy breve.';
    }
    if (errors.status) {
        return callback(null, {
            statusCode: 403,
            message: errors
        });
    }

    iocContainer.models.users.create({
        first_name: first_name,
        last_name: last_name,
        group_name: '',
        email: email,
        phone_number: phone_number,
        profile_image: 'https://s3.amazonaws.com/images.flashbid/profile.png',
        password: securityService.hash(password),
        balance: 0,
        term_condition: 1,
        status: 'Pending',
        role: 'Member',
        access_token: '',
        refresh_token: '',
        reset_token: '',
        confirm_token: securityService.generateString(10),
        bid_attempts: 0,
        money_owed: 0,
        created_at: new Date(),
        update_at: new Date(),
        device_id: device_id
    }).then(function (user) {

        currentUser = lodash.clone(user);

        mailService.sentEmail(currentUser.email, 'register', {
            first_name: currentUser.first_name,
            confirm_token: currentUser.confirm_token
        }).then(function (email) {
            return snsService.deliverMessage({}).then(function (snsMessage) {
                return callback(null, {
                    id: user.id,
                    access_token: user.access_token
                });
            });
        }).catch(function (e) {
            console.log('Registration Email Failed', e);
            return callback(null, {
                id: user.id,
                access_token: user.access_token
            });
        });

    }).catch(function (e) {
        console.log('User POST ERROR', e);
        if (e.name == 'SequelizeUniqueConstraintError') {
            callback(null, {
                statusCode: 403,
                message: {
                    email: 'Este correo electronico ya existe en nuestro sistema.'
                }
            })
        } else {
            callback(null, {
                statusCode: 403,
                message: {
                    email: 'registro fallido'
                }
            })
        }
    });
}
