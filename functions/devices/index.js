'use strict';
var iocContainer = require('./ioContainer');
var SecurityService = require('./service/SecurityService');
var securityService = new SecurityService();
var FirebaseService = require('./service/FirebaseService');
var firebaseService = new FirebaseService();

exports.handle = function (event, context, callback) {
    const userId = event.principalId;
    context.callbackWaitsForEmptyEventLoop = false;

    const body = event.body;
    const device_id = body && body.device_id ? body.device_id : '';
    let updateFields = {};

    if (device_id && device_id.length > 1) {
        updateFields.device_id = device_id;
    }

    iocContainer.models.users.find({
        where: {
            id: userId
        }
    }).then(function (user) {
        return user.updateAttributes(updateFields);
    }).then(function (user) {
        if (!user) {
            throw new Error('User not found ' + userId);
        } else {
            return callback(null, {success: true});
        }
    }).catch(function (e) {
        console.log('User EDIT ERROR', e);
        return callback(null, {success: false});
    });
}
