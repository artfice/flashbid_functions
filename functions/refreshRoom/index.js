'use strict';
var iocContainer = require('./ioContainer');
var Promise = require('bluebird');
var FirebaseService = require('./service/FirebaseService');
var SNSService = require('./service/SNSService');
var firebaseService = new FirebaseService();

exports.handle = function (event, context, callback) {
    context.callbackWaitsForEmptyEventLoop = false;
    console.log(JSON.stringify(event));
    console.log('Refresh Room');
    if (event.Records && event.Records.length > 0) {
        const promiseList = event.Records.map(function (message) {
            let sns = '{}';
            if (message.Sns) {
                sns = message.Sns.Message;
            }
            const payload = JSON.parse(sns);
            const today = new Date();
            const todayStr = today.toISOString().slice(0, 10);

            console.log('Payload', payload);

            let currentWinner = null;
            const id = payload.id;

            if (!payload.id) {
                return null;
            }

            const snsService = new SNSService('refreshItems');
            return snsService.deliverMessage({}).then(function (result) {
                return iocContainer.models.rooms.find({
                    where: {
                        id: id
                    }
                })
            }).then(function (result) {
                let room = result.toJSON();
                const current_time = new Date().getTime();
                const end_time = room.end_timestamp;
                const snsService = new SNSService('auctionEnd');
                
                if (room.state == 'Close') {
                    return firebaseService.write('room', room.id, room).then(function (results) {
                        return true;
                    });
                }
                // delete room.current_timestamp;
                console.log('room', room);
                if (end_time <= current_time) {

                    console.log('Throw Auction End EVENT');
                    
                    return snsService.deliverMessage({
                        room_id: id
                    }).then(function (msg) {
                        delete room.current_timestamp;
                        console.log('update room 1', room);
                        return firebaseService.updateSpecific('room', room.id, room).then(
                            function (result) {
                            return true;
                        });                    
                        // firebaseService.update('room', room.id, {
                        //     amount: room.amount,
                        //     bid_amount: room.bid_amount,
                        //     last_bid_name: room.last_bid_name,
                        //     winner_id: room.winner_id,
                        //     display_photo: room.display_photo,
                        //     display_name: room.display_name,
                        //     state: room.state
                        // });
                        // return true;
                    }).catch(function (err) {
                        console.log('RefreshRoom Error SNS ', err);
                        delete room.current_timestamp;
                        console.log('update room 2', room);
                        return firebaseService.updateSpecific('room', room.id, room).then(
                            function (result) {
                            return true;
                        });                    
                        // firebaseService.update('room', room.id, {
                        //     amount: room.amount,
                        //     bid_amount: room.bid_amount,
                        //     last_bid_name: room.last_bid_name,
                        //     winner_id: room.winner_id,
                        //     display_photo: room.display_photo,
                        //     display_name: room.display_name,
                        //     state: room.state
                        // });
                        // return true;
                    });
                } else {
                        delete room.current_timestamp;
                        console.log('update room 3', room);
                        return firebaseService.updateSpecific('room', room.id, room).then(
                            function (result) {
                            return true;
                        });                        
                        // firebaseService.update('room', room.id, {
                        //     amount: room.amount,
                        //     bid_amount: room.bid_amount,
                        //     last_bid_name: room.last_bid_name,
                        //     winner_id: room.winner_id,
                        //     display_photo: room.display_photo,
                        //     display_name: room.display_name,
                        //     state: room.state
                        // });
                        // return true;
                }


            }).catch(function (e) {
                console.log('REFRESH ROOM ERROR', e);
                return null;
            });
        });

        Promise.all(promiseList).then(function (allRooms) {
            console.log('all rooms');
            callback(null, allRooms);
        }).catch(function (err) {
            console.log('Refresh Room Error', err); 
            callback(err);
        });
    };
};