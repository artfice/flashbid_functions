'use strict';
var iocContainer = require('./ioContainer');
var lodash = require('lodash');
exports.handle = function (event, context, callback) {
context.callbackWaitsForEmptyEventLoop = false;
const id = event.path.id;
const userId = event.principalId;
let currentItem = null;
return iocContainer.models.items.find({
    where: {
        id: id
    }
}).then(function(item) {
    currentItem = lodash.clone(item.toJSON());
    currentItem.allowAuction = false;
    return iocContainer.models.item_user.find({
        where: { 
            item_id: id,
            user_id: userId
            }
    });
}).then(function(itemUser) {
    if (itemUser) {
        currentItem.allowAuction = true;
    }

    if (currentItem.winner_id > 0) {
        return iocContainer.models.users.find({
            where: {
                id: currentItem.winner_id
            }
        });
    } else {
        return false;
    }
}).then(function(user) {
    let item = currentItem;
    delete item.winner_code;
    if(user) {
        user = user.toJSON();

        item.winner = {
            first_name: user.first_name,
            last_name: user.last_name,
            profile_image: user.profile_image
        };
    }
    return callback(null, item);
}).catch( function (e) {
        console.log('Items GET ERROR', e);
        callback(null, {
            statusCode: 404,
            message: 'Objeto no encontrado'
        });
    });
}
