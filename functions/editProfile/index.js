'use strict';
var iocContainer = require('./ioContainer');
var SecurityService = require('./service/SecurityService');
var securityService = new SecurityService();
var FirebaseService = require('./service/FirebaseService');
var firebaseService = new FirebaseService();

exports.handle = function (event, context, callback) {
    const userId = event.principalId;
    context.callbackWaitsForEmptyEventLoop = false;

    const body = event.body;
    const first_name = body && body.first_name ? body.first_name : null;
    const last_name = body && body.last_name ? body.last_name : null;
    const email = body && body.email ? body.email : null;
    const password = body && body.password ? body.password : null;
    const profile_image = body && body.profile_image ? body.profile_image : null;
    const phone_number = body && body.phone_number ? body.phone_number : null;
    let updateFields = {};

    if (first_name) {
        updateFields.first_name = first_name;
    }

    if (last_name) {
        updateFields.last_name = last_name;
    }

    if (email) {
        updateFields.email = email;
    }

    if (profile_image) {
        updateFields.profile_image = profile_image;
    }

    if (phone_number) {
        updateFields.phone_number = phone_number;
    }

    if (password) {
        updateFields.password = securityService.hash(password);
    }

    iocContainer.models.users.find({
        where: {
            id: userId
        }
    }).then(function (user) {
        return user.updateAttributes(updateFields);
    }).then(function (user) {
        if (!user) {
            throw new Error('User not found ' + userId);
        } else {
            return firebaseService.write('user', user.id,
                securityService.firebaseUserObject(user)).then(function (result) {
                    return callback(null, user);
                }).catch(function (err) {
                    console.log('USER EDIT ERROR FIREBASE', err);
                    return callback(null, user);
                });
        }
    }).catch(function (e) {
        console.log('User EDIT ERROR', e);
        if (e.name == 'SequelizeUniqueConstraintError') {
            callback(null, {
                statusCode: 403,
                message: 'Este correo electronico ya existe en nuestro sistema.'
            });
        } else {
            callback(null, {
                statusCode: 403,
                message: 'Un Error en la actualizacion del usuario.'
            });
        }
    });
}
