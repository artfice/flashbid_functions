'use strict';
var iocContainer = require('./ioContainer');
var FirebaseService = require('./service/FirebaseService');
var firebaseService = new FirebaseService();
var SNSService = require('./service/SNSService');
var lodash = require('lodash');

exports.handle = function (e, context, callback) {
	context.callbackWaitsForEmptyEventLoop = false; 
	const today = new Date().getTime();
	const yesterday = today - (24 * 60 * 60000);
	const twoDayAgo = today - (2 * 24 * 60 * 60000);
	return iocContainer.models.rooms.findAll({
		where: {
			start_timestamp: {
				$lt: yesterday,
				$gt: twoDayAgo,
			}
		}
	}).then(function (result) {
		return Promise.all(result.map(function (room) {
			var roomId = lodash.clone(room.id);
			return firebaseService.write('room', roomId, {});
		}));
	}).then(function (result) {
		console.log(result);
		return callback(null, true);
	}).catch(function (e) {
		console.log('clear firebase ERROR', e);
		callback(null, {
			statusCode: 404,
			message: 'firebase error'
		});
	});
}