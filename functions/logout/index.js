'use strict';
var iocContainer = require('./ioContainer');
exports.handle = function (event, context, callback) {
    context.callbackWaitsForEmptyEventLoop = false;
    const userId = event.principalId;
    iocContainer.models.users.find({
        where: {
            id: userId
        }
    }).then(function (user) {
        return user.updateAttributes({
            access_token: '',
            refresh_token: ''
        });
    }).then(function (user) {
        callback(null, {
            statusCode: 200,
            message: 'Cerrar Sesion'
        });
    }).catch(function (e) {
        callback(e);
    });
};