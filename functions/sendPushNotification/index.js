'use strict';
var iocContainer = require('./ioContainer');
var lodash = require('lodash');
var Sequelize = require('sequelize');
var Promise = require('bluebird');
var FirebaseService = require('./service/FirebaseService');
var firebaseService = new FirebaseService();

exports.handle = function (event, context, cb) {
    console.log(JSON.stringify(event));
    if (event.Records && event.Records.length > 0) {
        const promiseList = event.Records.map(function (message) {
            let sns = '{}';
            if (message.Sns) {
                sns = message.Sns.Message;
            }

            const payload = JSON.parse(sns);

            console.log(payload);

            const user_id = payload.user_id;
            const title = payload.title;
            const description = payload.description;

            if (!user_id || !title || !description) {
                throw new Error('Missing User ID and title');
            }

            return iocContainer.models.users.find({
                where: {
                    id: user_id
                }
            }).then(function (user) {
                if (!user.device_id || user.device_id.length < 1){
                    return false;
                } else {
                    return firebaseService.sendPushNotification(title, description, user.device_id);
                }
            }).then(function (result) {
                    console.log('push', result);
                return result;
            }).catch(function (e) {
                console.log('Email Fail to send ', e);
                return e;
            });
        });

        Promise.all(promiseList).then(function (emails) {
            cb(null, emails);
        }).catch(function (err) {
            console.log('SEND EMAIL Error', err);
            cb(err);
        });        
    }
};