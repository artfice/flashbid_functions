'use strict';
var iocContainer = require('./ioContainer');
var SecurityService = require('./service/SecurityService');
const securityService = new SecurityService();

exports.handle = function (event, context, callback) {
    context.callbackWaitsForEmptyEventLoop = false;
    const body = event.body;
    const code = body.code;
    const password = body && body.password ? body.password : null;

    if (!password) {
        return callback(null, {
            statusCode: 403,
            message: 'Contraseña es muy breve.'
        });
    }

    iocContainer.models.users.find({
        where: {
            reset_token: code
        }
    }).then(function (user) {
        if (user) {
            return user.updateAttributes({
                reset_token: '',
                password: securityService.hash(password)
            });
        } else {
            throw new Error('Code not found');
        }
    }).then(function (user) {
        return callback(null, {
            statusCode: 200,
            message: 'Su contraseña ha sido reseteada. Puede Aceder la Aplicacion'
        });
    }).catch(function (e) {
        console.log('RESET PUT ERROR', e);
        return callback(null, {
            statusCode: 403,
            message: 'Reiniciar código inválido'
        });
        // callback(e);
    });
}
