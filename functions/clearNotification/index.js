'use strict';
var iocContainer = require('./ioContainer');
var FirebaseService = require('./service/FirebaseService');
var firebaseService = new FirebaseService();
 exports.handle = function(event, context, callback) { 
     context.callbackWaitsForEmptyEventLoop = false;
     const id = event.path.id;
    return firebaseService.write('notification', id, {}).then(function(result){
        return callback(null, true);
    }).catch( function (e) {
            console.log('clear notification ERROR', e);
            callback(null, {
                statusCode: 404,
                message: 'User not found'
            });
    });
}
