'use strict';
var iocContainer = require('./ioContainer');
var Promise = require('bluebird');
var FirebaseService = require('./service/FirebaseService');
var firebaseService = new FirebaseService();
var SecurityService = require('./service/SecurityService');
var securityService = new SecurityService();

exports.handle = function (event, context, callback) {
    context.callbackWaitsForEmptyEventLoop = false;
    console.log(JSON.stringify(event));

    console.log('Refresh User');

    if (event.Records && event.Records.length > 0) {
        const promiseList = event.Records.map(function (message) {
            let sns = '{}';
            if (message.Sns) {
                sns = message.Sns.Message;
            }

            const payload = JSON.parse(sns);
            const today = new Date();
            const todayStr = today.toISOString().slice(0, 10);

            console.log('Payload', payload);

            let currentWinner = null;
            const id = payload.id;

            if (!payload.id) {
                return null;
            }

            return iocContainer.models.users.find({
                where: {
                    id: id
                }
            }).then(function (user) {
                return firebaseService.write('user', user.id, securityService.firebaseUserObject(user)).then(function (result) {
                    return user;
                }).catch(function (err) {
                    console.log('REFRESH USER FIREBASE ERROR', err);
                    return user;
                });
            }).catch(function (e) {
                return null;
            });
        });

        Promise.all(promiseList).then(function (allUsers) {
            callback(null, allUsers);
        }).catch(function (err) {
            console.log('RefreshUser Error', err);
            callback(err);
        });
    }
}
