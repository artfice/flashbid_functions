'use strict';
var iocContainer = require('./ioContainer');
var lodash = require('lodash');
var Sequelize = require('sequelize');
var MailService = require('./service/MailService');
var mailService = new MailService(iocContainer.models);
var SNSService = require('./service/SNSService');

function auctionEnded(room) {
    const current_time = new Date().getTime();
    const end_time = room.end_timestamp;
    console.log('current time', current_time);
    console.log('end time', end_time);
    console.log('Result', (end_time <= current_time));
    const condition = (end_time <= current_time) &&
        room.winner_id == 0;
    return condition;
}

function findWinner(room) {
    const bidHistory = room.bid_history;
    const bids = bidHistory.split(';').filter(function (bid) {
        return bid.length > 0;
    });
    //get Last Bidder
    if (bids.length > 0) {
        const finalBid = bids[bids.length - 1];
        const finalPayload = finalBid.split(':');
        const user_id = finalPayload[0];
        return user_id;
    } else {
        return false;
    }
}
/*
Steps:
1. Get Room
2. If Room Close -> Exit
3. If Auction Still Live -> Exit
4. Get System
5. Get Item
6. If No winner found Room, Throw Event AUCTIONLOST
7. If Winner - Take Money
8. Update System Earning
9. Update Item
10. Update Room
11. Update User
12. Save Transaction
13. Loop through item_user -> trigger event AuctionEndOne
 */
exports.handle = function (event, context, cb) {
    console.log(JSON.stringify(event));
    console.log('AUCTION ENDED');
    if (event.Records && event.Records.length > 0) {
        const promiseList = event.Records.map(function (message) {
            let sns = '{}';
            if (message.Sns) {
                sns = message.Sns.Message;
            }
            const payload = JSON.parse(sns);

            console.log('Payload', payload);

            const id = payload.room_id;

            if (!id) {
                return cb(null, {
                    message: "Not a Room to process"
                });
            }

            let lastAmount = 0;
            let difference = 0;
            let oldBalance = 0;

            let done = false;
            let noWinners = false;
            let currentRoom = false;
            let currentSystem = false;
            let currentWinner = null;
            let currentItem = null;            
            let winner = null;
            let lastBidder = 0;
            let notes = '';
            let hasMoney = 0;

            const today = new Date();
            const todayStr = today.toISOString().slice(0, 10);

            return iocContainer.db.transaction({
                isolationLevel: Sequelize.Transaction.ISOLATION_LEVELS.SERIALIZABLE
            }).then(function (t) {
                const promiseList = [
                    iocContainer.models.rooms.find({ where: { id: id } }, { transaction: t }),
                    iocContainer.models.system.find({ where: { id: 1 } }, { transaction: t }),
                    iocContainer.db.query('SELECT user_id, amount FROM item_bid WHERE room_id = ' + id + ' ORDER BY amount DESC LIMIT 1', { transaction: t })
                ];
                return Promise.all(promiseList).then(function (results) {
                    currentRoom = lodash.clone(results[0]);
                    currentSystem = lodash.clone(results[1]);
                    const bidHistory = results[2];
                    // If room close but no one bidded
                    if (currentRoom.state == 'Open' && currentRoom.initial_bid == currentRoom.amount) {
                        noWinners = true;
                        throw new Error('Auction Lost');
                    }
                    // 2. If Room Close -> Exit
                    if (currentRoom.state == 'Close') {
                        throw new Error('Auction is Closed');
                    }
                    // 3. If Auction Still Live -> Exit
                    if (!auctionEnded(currentRoom)) {
                        throw new Error('Auction Still Going');
                    }                    
                    if (bidHistory && bidHistory[0].length > 0) {
                        console.log('BID History', bidHistory[0]);
                        lastBidder = bidHistory[0][0].user_id;
                        lastAmount = bidHistory[0][0].amount;
                    } else {
                        //Auction Lost
                        // 6. If No winner found Room
                        noWinners = true;
                        throw new Error('Auction Lost');
                    }
                    // console.log('Current Room', currentRoom);
                    console.log('LAST Bidder', lastBidder);
                    return iocContainer.models.users.find({ where: { id: lastBidder } }, { transaction: t });
                }).then(function (user) {
                    // 7. If Winner - Take Money
                    winner = lodash.clone(user);
                    difference = winner.balance - lastAmount;
                    oldBalance = winner.balance;
                    let updateFields = {};
                    if (difference < 0) {
                        hasMoney = 2;
                        updateFields = {
                            balance: 0,
                            money_owed: Math.abs(difference)
                        };
                        notes = '.Usuario debe dinero por falta de balance suficiente.';
                    } else if (difference == 0) {
                        hasMoney = 1;
                        updateFields = {
                            balance: difference
                        };
                    } else {
                        hasMoney = 1;
                        updateFields = {
                            balance: difference
                        };
                    }
                    // 8. Update System Earning
                    // 9. Update Item
                    // 10. Update Room
                    // 11. Update User
                    // 12. Save Transaction
                    const promiseList = [
                        iocContainer.models.items.update({ status: 'Complete', winner_id: winner.id, final_bid: lastAmount }, { where: { id: currentRoom.item_id }, transaction: t }),
                        iocContainer.models.system.update({ earned: currentSystem.earned + lastAmount }, { where: { id: 1 }, transaction: t }),
                        iocContainer.models.rooms.update({ state: 'Close', display_photo: winner.profile_image, display_name: winner.first_name + ' ' + winner.last_name, winner_id: winner.id }, { where: { id: currentRoom.id }, transaction: t }),
                        iocContainer.models.users.update(updateFields, { where: { id: winner.id }, transaction: t }),
                        iocContainer.models.transactions.create({
                            action: 'Auction',
                            user_id: winner.id,
                            item_id: currentRoom.item_id,
                            amount: lastAmount,
                            userOldTotal: oldBalance,
                            userTotal: difference,
                            systemOldTotal: currentSystem.earned,
                            systemTotal: currentSystem.earned + lastAmount,
                            notes: 'Usuario ' + winner.id + ' ha ganado la subasta del articulo ' + currentRoom.item_id + notes,
                            code: '',
                            created_at: todayStr,
                            updated_at: today
                        }, { transaction: t })
                    ];
                    return Promise.all(promiseList);
                }).then(function (result) {
                    return iocContainer.models.items.find({ where: { id: currentRoom.item_id } }, { transaction: t });
                }).then(function (item) {
                    currentItem = lodash.clone(item);
                    //13. Send Email to Winner
                    if (hasMoney == 1) {
                        return mailService.sentEmail(winner.email, 'winner-code', {
                            first_name: winner.first_name,
                            title: currentItem.title,
                            item_image: currentItem.image,
                            winner_code: currentItem.winner_code,
                            winner_instructions: currentItem.winner_instructions
                        });
                    } else if (hasMoney == 2) {
                        return mailService.sentEmail(winner.email, 'winner-code-no-money', {
                            first_name: winner.first_name,
                            title: currentItem.title,
                            item_image: currentItem.image,
                            winner_code: currentItem.winner_code,
                            winner_instructions: currentItem.winner_instructions,
                            money_owed: Math.abs(difference)

                        });
                    } else {
                        console.log('Cant be hasNoMoney and win');
                        return false;
                    }
                }).then(function (email) {
                    // 13. Loop through item_user
                    return iocContainer.models.item_user.findAll({
                        where: { item_id: currentItem.id }
                    });
                }).then(function (itemUsers) {
                    const snsService = new SNSService('auctionEndOne');
                    return Promise.all(itemUsers.map(function (itemUser) {
                        return snsService.deliverMessage({
                            id: itemUser.user_id,
                            winner_id: winner.id,
                            winner_first_name: winner.first_name + ' ' + winner.last_name,
                            title: currentItem.title,
                            item_image: currentItem.image
                        });
                    }));
                }).then(function (result) {
                    t.commit();
                    const snsService = new SNSService('refreshRoom');
                    const snsItemService = new SNSService('refreshItems');
                    const snsUserService = new SNSService('refreshUser');
                    return Promise.all([
                        snsService.deliverMessage({ id: currentRoom.id }),
                        snsItemService.deliverMessage({}),
                        snsUserService.deliverMessage({ id: winner.id })
                    ]).then(function (result) {
                        console.log("auction result", result);
                        return cb(null, {
                            message: 'Auction has Closed'
                        });
                    }).catch(function (err) {
                        console.log('Auction End SNS Closed Error', e);
                        return cb(null, {
                            message: 'Auction has Closed'
                        });
                    });
                }).catch(function (err) {
                    console.log('Auction Error ', err);
                    t.rollback();
                    if (noWinners) {
                        const snsItemService = new SNSService('auctionLost');
                        return Promise.all([
                        snsItemService.deliverMessage({
                            id: currentRoom.id
                        })]).then(function (result) {
                            return cb(null, result);
                        }).catch(function (error) {
                            console.log('AUCTION LOST ONE ERROR', error);
                            return cb(null, error);
                        });
                    } else {
                        cb(err);
                    }
                });
            });

            Promise.all(promiseList).then(function (auctionEnd) {
                callback(null, auctionEnd);
            }).catch(function (err) {
                console.log('Auction End Error', err);
                callback(err);
            });
        });
    }
};