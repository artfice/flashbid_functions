/*
Steps
1. Get User, Item, System
2. If User Broke -> Exit
3. If Item Not Active -> Exit
4. Update User Balance
5. Update Firebase User
6. Create Transaction
7. Update System Earning
8. Write item_user record
9. Update Item
10. If ticket told reached, throw event if lottery
11. If ticket told reached, throw event if auction

*/
var Promise = require('bluebird');
var index = require('./index');

var list = [
    new Promise (function(resolve, reject){
        const event = {
            principalId: 30,
            path: {
                id: 24
            }
        };
        index.handle(event, {}, function(e, data){
            // console.log('Error', e);
            console.log('User Broke Answer', data);
            resolve(data);
        })
    }),
    new Promise (function(resolve, reject){
        const event = {
            principalId: 31,
            path: {
                id: 25
            }
        };
        index.handle(event, {}, function(e, data){
            // console.log('Error', e);
            console.log('Item Not Active Answer', data);
            resolve(data);
        })
    }),
    new Promise (function(resolve, reject){
        const event = {
            principalId: 33,
            path: {
                id: 24
            }
        };
        index.handle(event, {}, function(e, data){
            // console.log('Error', e);
            console.log('Has Money Answer', data);
            resolve(data);
        })
    })            
];

Promise.all(list).then(function(results){
    console.log(results);
});
