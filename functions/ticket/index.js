'use strict';
var iocContainer = require('./ioContainer');
var lodash = require('lodash');
var Promise = require('bluebird');
var MailService = require('./service/MailService');
var FirebaseService = require('./service/FirebaseService');
var SNSService = require('./service/SNSService');
var SecurityService = require('./service/SecurityService');
var mailService = new MailService(iocContainer.models);
var firebaseService = new FirebaseService();
var securityService = new SecurityService();

/*
Steps
1. Get User, Item, System
2. If User Broke -> Exit
3. If Item Not Active -> Exit
4. Update User Balance
5. Update Firebase User
6. Create Transaction
7. Update System Earning
8. Write item_user record
9. Update Item
10. If ticket told reached, throw event if lottery
11. If ticket told reached, throw event if auction

*/
exports.handle = function (event, context, callback) {
    context.callbackWaitsForEmptyEventLoop = false;

    const userId = event.principalId;
    const id = event.path.id;

    let currentUser = null;
    let currentSystem = null;
    let currentItem = null;
    let insufficentFunds = false;
    let notActiveItem = false;
    let userBalance = 0;
    let userHasTicket = false;

    const today = new Date();
    const todayStr = today.toISOString().slice(0, 10);

    iocContainer.db.transaction({
        isolationLevel: iocContainer.isolationLevel
    }).then(function (t) {
        let promiseList = [
            iocContainer.models.users.find({ where: { id: userId } }, { transaction: t }),
            iocContainer.models.items.find({ where: { id: id } }, { transaction: t }),
            iocContainer.models.system.find({ where: { id: 1 } }, { transaction: t }),
            iocContainer.models.item_user.find({ where: { user_id: userId, item_id: id} }, { transaction: t })
        ];
        //1. Get User, Item, System
        Promise.all(promiseList).then(function (results) {
            currentUser = lodash.clone(results[0]);
            currentItem = lodash.clone(results[1]);
            currentSystem = lodash.clone(results[2]);
            userBalance = lodash.clone(currentUser.balance);
            userHasTicket = (results[3]) ? true : false;

            if (userHasTicket) {
                throw new Error('User Bought a ticket already');
            }

            //2. If User Broke -> Exit
            if (currentUser.balance < currentItem.ticket_amount) {
                insufficentFunds = true;
                throw new Error('Insufficient Funds');
            }

            // 3. If Item Not Active -> Exit
            if (currentItem.status != 'Active') {
                notActiveItem = true;
                throw new Error('Not Active Item');
            }
            //4. Update User Balance
            return currentUser.updateAttributes({
                balance: userBalance - currentItem.ticket_amount
            }, { transaction: t });
        }).then(function (user) {
            //5. Update Firebase User
            return firebaseService.write('user', user.id, securityService.firebaseUserObject(user));
        }).then(function (result) {
            //6. Create Transaction
            return iocContainer.models.transactions.create({
                action: 'Ticket',
                user_id: userId,
                item_id: id,
                amount: currentItem.ticket_amount,
                userOldTotal: userBalance,
                userTotal: userBalance - currentItem.ticket_amount,
                systemOldTotal: currentSystem.earned,
                systemTotal: currentSystem.earned + currentItem.ticket_amount,
                notes: 'Usuario ' + userId + ' compro un boleto para el articulo ' + id,
                code: '',
                created_at: todayStr,
                updated_at: today
            }, { transaction: t });
        }).then(function (result) {
            // 7. Update System Earning
            return currentSystem.updateAttributes({
                earned: currentSystem.earned + currentItem.ticket_amount
            }, { transaction: t });
        }).then(function (result) {
            // 8. Write item_user record
            return iocContainer.models.item_user.create({
                user_id: userId,
                item_id: id
            }, { transaction: t });
        }).then(function (result) {
            // 9. Update Item
            const ticketSold = currentItem.ticket_sold + 1;

            let updateFields = {
                ticket_sold: ticketSold
            };
            return currentItem.updateAttributes(updateFields, { transaction: t });
        }).then(function (item) {
            if (item.ticket_sold >= item.ticket_total) {
                if (item.type == 'l') {
                    console.log('Lottery Start Event');
                    const snsService = new SNSService('lotteryWon');
                    return snsService.deliverMessage({
                        id: item.id
                    });
                } else {
                    console.log('Auction Start Event');
                    const snsService = new SNSService('auctionStart');
                    return snsService.deliverMessage({
                        id: item.id
                    });
                }
            } else {
                return false;
            }
        }).then(function (result) {
            return iocContainer.models.history.create({
                user_id: userId,
                message: 'Agregar alerta de oferta "Flash" al articulo ' + currentItem.title,
                read: 0,
                created_at: todayStr,
                update_at: today.getTime()
            }, { transaction: t });        
        }).then(function (result) {
            const snsService = new SNSService('refreshItems');
            return snsService.deliverMessage({});
        }).then(function (result) {
            console.log('Ticket Result', result);
            t.commit();
            return callback(null, {
                statusCode: 200,
                message: 'Agregar alerta de oferta "Flash" al articulo '  + currentItem.title
            });
        }).catch(function (err) {
            console.log('Bid Ticket Error ', err);
            t.rollback();
            if (insufficentFunds) {
                return callback(null, {
                    statusCode: 403,
                    message: 'Usted no tiene balance disponible para esta operación.'
                });
            } else if (notActiveItem) {
                return callback(null, {
                    statusCode: 403,
                    message: 'Su elemento no está activo'
                });
            } else if (userHasTicket) {
                return callback(null, {
                    statusCode: 403,
                    message: 'Ya tiene una alerta de oferta'
                });
            } else {
                return callback(null, {
                    statusCode: 403,
                    message: 'No puede hacer una puja por este artículo en este momento.'
                });
            }
        });

    });
};