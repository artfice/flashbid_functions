'use strict';
var iocContainer = require('./ioContainer');
var FirebaseService = require('./service/FirebaseService');
var SNSService = require('./service/SNSService');
var firebaseService = new FirebaseService();
exports.handle = function (event, context, callback) {
    context.callbackWaitsForEmptyEventLoop = false;

    const id = event.body.id;
    return iocContainer.models.rooms.find({
        where: {
            id: id
        }
    }).then(function (room) {
        // console.log('Room', room.toJSON());
        room = room.toJSON();

        const current_time = new Date().getTime();
        const end_time = room.end_timestamp;

        if (end_time <= current_time) {
            console.log('Throw Auction End');
            const snsService = new SNSService('auctionEnd');
            return snsService.deliverMessage({
                room_id: id
            }).then(function (msg) {
                return callback(null, room);
            }).catch(function (err) {
                console.log('RefreshRoom Error SNS ', err);
                return callback(null, room);
            })
        } else {
            return callback(null, room);
        }
    }).catch(function (e) {
        console.log('GET ROOM ERROR', e);
        return callback(null, {
            statusCode: 404,
            message: 'Sala de subastas no encontrada'
        });
    });
}
