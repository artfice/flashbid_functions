'use strict';
var iocContainer = require('./ioContainer');
var PaginationService = require('./service/PaginationService');

exports.handle = function (event, context, callback) {
    const userId = event.principalId;
    context.callbackWaitsForEmptyEventLoop = false;
    const pagination = new PaginationService(event.query.page, event.query.numItems);

    let where = {
        user_id: userId
    };

    let query = {
        attributes: [[iocContainer.db.fn('COUNT', iocContainer.db.col('id')), 'num']],
        order: [
            ['id', 'DESC']
        ],
        where: where
    };

    iocContainer.models.history.findAll(query).then(
        function (countResult) {
            if (countResult.length > 0) {
                pagination.setCount(countResult[0].dataValues.num);
                delete query.attributes;
                query.offset = pagination.getOffset();
                query.limit = pagination.getLimit();
                return iocContainer.models.history.findAll(query);
            } else {
                callback(null, {
                    statusCode: 200,
                    items: pagination.getItems(),
                    page: pagination.getPage(),
                    num_items: pagination.getCount(),
                    num_pages: pagination.getNumPages()
                });
            }
        }).then( function (items) {
            pagination.setItems(items);
            callback(null, {
                statusCode: 200,
                items: pagination.getItems(),
                page: pagination.getPage(),
                num_items: pagination.getCount(),
                num_pages: pagination.getNumPages()
            });
    }).catch( function (e) {
        console.log('History GET ERROR', e);
        callback(null, {
            statusCode: 404,
            message: 'No se pudo recuperar el historial'
        });
    });    
}
