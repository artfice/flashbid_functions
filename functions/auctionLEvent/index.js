'use strict';
var iocContainer = require('./ioContainer');
var Promise = require('bluebird');
var lodash = require('lodash');
var MailService = require('./service/MailService');
var SNSService = require('./service/SNSService');
var mailService = new MailService(iocContainer.models);
/*
AuctionLost
1. Get Users
2. Send Notification
 */
exports.handle = function (event, context, callback) {
    context.callbackWaitsForEmptyEventLoop = false;

    console.log(JSON.stringify(event));

    console.log('Auction Lost');

    if (event.Records && event.Records.length > 0) {
        const promiseList = event.Records.map(function (message) {
            let sns = '{}';
            if (message.Sns) {
                sns = message.Sns.Message;
            }
            const payload = JSON.parse(sns);
            const today = new Date();
            const todayStr = today.toISOString().slice(0, 10);

            console.log('Payload', payload);

            let currentWinner = null;
            let currentItem = null;
            let currentRoom = null;
            const id = payload.id;

            if (!payload.id) {
                return null;
            }

            //1. Get Item
            return iocContainer.models.rooms.find({
                where: { id: id }
            }).then(function (room) {
                currentRoom = lodash.clone(room);
                const promiseList = [
                    iocContainer.models.rooms.update({ state: 'Close' }, { where: { id: id }}),
                    iocContainer.models.items.update({ status: 'Lost' }, { where: { id: currentRoom.item_id }})
                ];
            }).then(function (result) {
                const snsService = new SNSService('refreshRoom');
                return snsService.deliverMessage({ id: currentRoom.id });
            }).then(function (result) {
                return iocContainer.models.item_user.findAll({
                    where: { item_id: currentRoom.item_id }
                });
            }).then(function (itemUsers) {
                const snsService = new SNSService('auctionLostOne');
                return Promise.all(itemUsers.map(function (itemUser) {
                    return snsService.deliverMessage({
                        id: itemUser.user_id,
                        title: currentRoom.item_title,
                        item_image: currentRoom.item_image
                    });
                }));
            }).then(function (messages) {
                return true;
            }).catch(function (error) {
                console.log('Auction LOST ERROR ID', id);
                console.log('Auction LOST ERROR', error);
                return null;
            });
        });

        Promise.all(promiseList).then(function (auctionLost) {
            callback(null, auctionLost);
        }).catch(function (err) {
            console.log('Auction Lost Error', err);
            callback(err);
        });
    }
}
