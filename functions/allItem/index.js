'use strict';
var iocContainer = require('./ioContainer');
var FirebaseService = require('./service/FirebaseService');
var firebaseService = new FirebaseService();
 exports.handle = function(event, context, callback) { 
     context.callbackWaitsForEmptyEventLoop = false;
     iocContainer.models.items.findAll({
         where: {
            $or: [{
                status: 'Active'
            }, {
                status: 'Flash'
            }]
        }
     }).then(function (items) {
            return firebaseService.write('allItems', 1, items.map(function(item){
                var item = item.toJSON();
                delete item.winner_code;
                delete item.winner_instructions;
                delete item.vendor_id;
                return item;
            })).then(function(result){
                return callback(null, items);
            });
        }).catch( function (e) {
            console.log('All Items GET ERROR', e);
            callback(null, {
                statusCode: 404,
                message: 'Items not found'
            });
    });
}
