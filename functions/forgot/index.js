'use strict';
var iocContainer = require('./ioContainer');
var SecurityService = require('./service/SecurityService');
var MailService = require('./service/MailService');
var lodash = require('lodash');
var mailService = new MailService(iocContainer.models);
const securityService = new SecurityService();

exports.handle = function (event, context, callback) {
    context.callbackWaitsForEmptyEventLoop = false;
    const body = event.body;
    const email = body && body.email ? body.email : null;
    let currentUser = null;

    if (!email) {
        return callback(null, {
            statusCode: 403,
            message: 'Ingrese un correo electrónico Valido'
        });
    }

    iocContainer.models.users.find({
        where: {
            email: email
        }
    }).then(function (user) {

        currentUser = lodash.clone(user);

        return user.updateAttributes({
            reset_token: securityService.generateString(10)
        });
    }).then(function (user) {
        return mailService.sentEmail(currentUser.email, 'reset-password', {
            first_name: currentUser.first_name,
            email: currentUser.email,
            reset_token: user.reset_token
        }).then(function (email) {
            return callback(null, {
                statusCode: 200,
                message: 'Hemos enviado tu código para resetear la contraseña'
            });
        }).catch(function (e) {
            console.log('Forgot Email Failed', e);
            return callback(null, {
                statusCode: 200,
                message: 'Hemos enviado tu código para resetear la contraseña'
            });            
        });
    }).catch(function (e) {
        console.log('Forgot PUT ERROR', e);
        callback(null, {
            statusCode: 403,
            message: 'No se pudo restablecer la contraseña'
        });
    });
}
