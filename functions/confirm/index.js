'use strict';
var iocContainer = require('./ioContainer');
exports.handle = function (event, context, callback) {
    context.callbackWaitsForEmptyEventLoop = false;
    const confirm = event.path.code;
    iocContainer.models.users.find({
        where: {
            confirm_token: confirm
        }
    }).then(function (user) {
        if (user.status != 'Pending' || user.confirm_token.length < 1) {
            return {
                statusCode: 403,
                message: 'Ya confirmó su cuenta.'
            };
        } else {
            return user.updateAttributes({
                status: 'Active',
                confirm_token: ''
            });
        }
    }).then(function (user) {
        if (user.statusCode == 403) {
            return callback(null, user);
        } else {
            return callback(null, 'Confirma tu cuenta. Puedes ingresar ahora.');
        }
    }).catch(function (e) {
        console.log('Confirm User ERROR', e);
        callback(e);
    });
}
