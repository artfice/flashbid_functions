'use strict';
var iocContainer = require('./ioContainer');
var FirebaseService = require('./service/FirebaseService');
var firebaseService = new FirebaseService();

exports.handle = function (event, context, callback) {
    context.callbackWaitsForEmptyEventLoop = false;
    iocContainer.models.questions.findAll({}).then(
        function (questions) {
            firebaseService.write('questions', 1,questions.map(function(question){
                return question.toJSON();
            })).then(function(result){
                return callback(null, questions);
            });
        }).catch(function (e) {
            console.log('Question GET ERROR', e);
            callback(null, {
                statusCode: 404,
                message: 'Question not found'
            });
        });
};