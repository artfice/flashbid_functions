'use strict';
var iocContainer = require('./ioContainer');
var FirebaseService = require('./service/FirebaseService');
var firebaseService = new FirebaseService();
var SNSService = require('./service/SNSService');

exports.handle = function(e, context, callback) {
  context.callbackWaitsForEmptyEventLoop = false;
  const snsService = new SNSService('auctionCountDown');
  snsService.deliverMessage({
		id: 85
	}).then(function(body){
		console.log(body);
		callback(null, body);
	});
}
