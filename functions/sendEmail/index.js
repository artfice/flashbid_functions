'use strict';
var iocContainer = require('./ioContainer');
var lodash = require('lodash');
var Sequelize = require('sequelize');
var MailService = require('./service/MailService');
var mailService = new MailService(iocContainer.models);
var Promise = require('bluebird');

exports.handle = function (event, context, cb) {
    console.log(JSON.stringify(event));
    if (event.Records && event.Records.length > 0) {
        const promiseList = event.Records.map(function (message) {
            let sns = '{}';
            if (message.Sns) {
                sns = message.Sns.Message;
            }

            const payload = JSON.parse(sns);

            console.log(payload);

            const to = payload.to;
            const user_id = payload.user_id;
            const template = payload.template;
            const data = payload.data || {};

            if (!user_id || !template) {
                throw new Error('Missing User ID and template');
            }

            return mailService.sentEmailToUser(user_id, template, data).then(
                function (body) {
                    console.log('email', user_id, template, body);
                    return body;
                }).catch(function (e) {
                    console.log('Email Fail to send ', e);
                    return e;
                });
        });

        Promise.all(promiseList).then(function (emails) {
            cb(null, emails);
        }).catch(function (err) {
            console.log('SEND EMAIL Error', err);
            cb(err);
        });        
    }
};