'use strict';
var iocContainer = require('./ioContainer');
var bluebird = require('bluebird');
var lodash = require('lodash');
var MailService = require('./service/MailService');
var SNSService = require('./service/SNSService');
var mailService = new MailService(iocContainer.models);
var FirebaseService = require('./service/FirebaseService');
var firebaseService = new FirebaseService();
/*
AuctionStart
1. Get Item
2. Create Room
3. Write Room to firebase
4. Write Room ID to Item
5. Refresh Item
6. Throw event for auctionOne
 */
exports.handle = function (event, context, callback) {

    context.callbackWaitsForEmptyEventLoop = false;
    console.log(JSON.stringify(event));
    console.log('Auction Start');
    const snsService = new SNSService('sendEmail');
    if (event.Records && event.Records.length > 0) {
        const promiseList = event.Records.map(function (message) {
            let sns = '{}';
            if (message.Sns) {
                sns = message.Sns.Message;
            }
            const payload = JSON.parse(sns);
            const today = new Date();
            const todayStr = today.toISOString().slice(0, 10);

            console.log('Payload', payload);

            let currentRoom = null;
            let currentItem = null;
            const id = payload.id;

            if (!payload.id) {
                return null;
            }
            //1. Get Item
            return iocContainer.models.items.find({
                where: { id: id }
            }).then(function (item) {
                currentItem = lodash.clone(item);

                if (item.status == 'Active' &&
                    item.ticket_sold >= item.ticket_total) {
                    //2. Create Room
                    const start_time = new Date().getTime();
                    const flash_time = start_time + (3 * 60000);
                    const end_time = start_time + (4 * 60000);
                    // const flash_time = start_time + (1 * 60000);
                    // const end_time = start_time + (1.5 * 60000);
                    return iocContainer.models.rooms.create({
                        start_timestamp: start_time,
                        flash_timestamp: flash_time,
                        end_timestamp: end_time,
                        item_id: id,
                        item_title: currentItem.title,
                        item_image: currentItem.secondary_image,
                        initial_bid: currentItem.initial_bid,
                        amount: currentItem.initial_bid,
                        bid_amount: currentItem.bid_amount,
                        winner_id: 0,
                        display_photo: '',
                        display_name: '',
                        state: 'Open'
                    });
                } else {
                    throw new Error('Item Not ready');
                }
            }).then(function (room) {
                //3. Write Room to firebase
                currentRoom = lodash.clone(room);
                var roomJson = room.toJSON();
                roomJson.current_timestamp = 240;
                return firebaseService.write('room', room.id, roomJson);
            }).then(function (messages) {
                //4. Write Room ID to Item
                return currentItem.updateAttributes({
                    room_id: currentRoom.id,
                    status: 'Flash'
                });
            }).then(function (item) {
                //5. Refresh Item
                const snsService = new SNSService('refreshItems');
                return snsService.deliverMessage({});
            }).then(function (bodies) {
                //6. Get all item_user
                return iocContainer.models.item_user.findAll({
                    where: { item_id: id }
                });
            }).then(function (itemUsers) {
                // 7. Throw event for auctionOne
                const snsService = new SNSService('auctionOne');
                return Promise.all(itemUsers.map(function (itemUser) {
                    return snsService.deliverMessage({
                        id: itemUser.user_id,
                        item_id: currentItem.id,
                        title: currentItem.title,
                        item_image: currentItem.image
                    });
                }));
            }).then(function (messages) {
                console.log('Perform Auction Count Down', currentRoom.id);
                const snsCountService = new SNSService('auctionCountDown');
                return snsCountService.deliverMessage({
                    id: currentRoom.id
                });
            }).then(function (results) {
                console.log(results);
                return true;
            }).catch(function (error) {
                console.log('Auction Start ERROR ID', id);
                console.log('Auction Start ERROR', error);
                return null;
            });
        });
        Promise.all(promiseList).then(function (allLottery) {
            callback(null, allLottery);
        }).catch(function (err) {
            console.log('Auction Start Error', err);
            callback(err);
        });
    }
}
