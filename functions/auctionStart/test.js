/*
AuctionStart
1. Get Item
2. Create Room
3. Write Room to firebase
4. Write Room ID to Item
5. Refresh Item
6. Throw event for auctionOne
 */
var Promise = require('bluebird');
var index = require('./index');

var list = [           
    new Promise(function (resolve, reject) {
        //NOT ACTIVE ITEM
        const event = {
            "Records": [
                {
                    "EventSource": "aws:sns",
                    "EventVersion": "1.0",
                    "EventSubscriptionArn": "arn:aws:sns:us-east-1:954804258659:auctionEnd:6b8a5421-f990-4cd9-9480-5cfc6e3956e5",
                    "Sns": {
                        "Type": "Notification",
                        "MessageId": "7d82b2c3-b8eb-508a-9a40-e201076ca80c",
                        "TopicArn": "arn:aws:sns:us-east-1:954804258659:auctionOne",
                        "Subject": null,
                        "Message": "{\"id\":25}",
                        "Timestamp": "2016-12-08T01:54:15.396Z",
                        "SignatureVersion": "1",
                        "Signature": "hkUKfmk2J9jkGHlpP2r5vCPDuCTdP3BRTG7og2qAp9zKsQ03DqHqVLxpHicoZ0WovOPqsJHArPwgwR5lYXQkMHtMAQYUILq0qRTiuNckrAKK5V4X/xwZQJx67AeOz+lexFyus8FO4kB2mJ8YA/B2KWUhdmuz/xwaGhMgW5QxJKeAw0B3GBkYn06q4QhNb0hGl7lvcWfVzUq7WC6O7HKl8YHNX8GI/YOHIb4/94EYCkFM6nYzJUtuolGgqNKVmpy2CalHVZd+uCz44rN/WbnWxAQf2fLOEUWa7QPI8Zt4n2DsKR4tMST9E6grhteVjT6r9ezeOZhVeG/K1C2gjr2tXg==",
                        "SigningCertUrl": "https://sns.us-east-1.amazonaws.com/SimpleNotificationService-b95095beb82e8f6a046b3aafc7f4149a.pem",
                        "UnsubscribeUrl": "https://sns.us-east-1.amazonaws.com/?Action=Unsubscribe&SubscriptionArn=arn:aws:sns:us-east-1:954804258659:auctionEnd:6b8a5421-f990-4cd9-9480-5cfc6e3956e5",
                        "MessageAttributes": {}
                    }
                }
            ]
        };
        index.handle(event, {}, function (e, data) {
            // console.log('Error', e);
            console.log('User Broke Answer', data);
            resolve(data);
        })
    }),
    new Promise(function (resolve, reject) {
        //Check firebase
        //Check Item is Flash
        //Check RefreshItem Logs
        //Check item_user
        //Check auctionOne send email and sent notification
        const event = {
            "Records": [
                {
                    "EventSource": "aws:sns",
                    "EventVersion": "1.0",
                    "EventSubscriptionArn": "arn:aws:sns:us-east-1:954804258659:auctionEnd:6b8a5421-f990-4cd9-9480-5cfc6e3956e5",
                    "Sns": {
                        "Type": "Notification",
                        "MessageId": "7d82b2c3-b8eb-508a-9a40-e201076ca80c",
                        "TopicArn": "arn:aws:sns:us-east-1:954804258659:auctionOne",
                        "Subject": null,
                        "Message": "{\"id\":26}",
                        "Timestamp": "2016-12-08T01:54:15.396Z",
                        "SignatureVersion": "1",
                        "Signature": "hkUKfmk2J9jkGHlpP2r5vCPDuCTdP3BRTG7og2qAp9zKsQ03DqHqVLxpHicoZ0WovOPqsJHArPwgwR5lYXQkMHtMAQYUILq0qRTiuNckrAKK5V4X/xwZQJx67AeOz+lexFyus8FO4kB2mJ8YA/B2KWUhdmuz/xwaGhMgW5QxJKeAw0B3GBkYn06q4QhNb0hGl7lvcWfVzUq7WC6O7HKl8YHNX8GI/YOHIb4/94EYCkFM6nYzJUtuolGgqNKVmpy2CalHVZd+uCz44rN/WbnWxAQf2fLOEUWa7QPI8Zt4n2DsKR4tMST9E6grhteVjT6r9ezeOZhVeG/K1C2gjr2tXg==",
                        "SigningCertUrl": "https://sns.us-east-1.amazonaws.com/SimpleNotificationService-b95095beb82e8f6a046b3aafc7f4149a.pem",
                        "UnsubscribeUrl": "https://sns.us-east-1.amazonaws.com/?Action=Unsubscribe&SubscriptionArn=arn:aws:sns:us-east-1:954804258659:auctionEnd:6b8a5421-f990-4cd9-9480-5cfc6e3956e5",
                        "MessageAttributes": {}
                    }
                }
            ]
        };
        index.handle(event, {}, function (e, data) {
            // console.log('Error', e);
            console.log('User Broke Answer', data);
            resolve(data);
        })
    })
];

Promise.all(list).then(function (results) {
    console.log(results);
});
