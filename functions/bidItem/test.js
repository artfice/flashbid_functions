/*
New Steps
1. Get User ID, Room
2. If current time < flash time -> Exit throw auctionEnd 
3. If user balance is < 25% of nextBidPrice -> Exit say they don't have enough credit to continue
4. If last bidder was them, they can't bid again
SELECT user_id, amount FROM item_bid WHERE item_id = 1 ORDER BY amount DESC LIMIT 1
5. Write to DB the bid
6. Update room
7. call refreshRoom
 */
var Promise = require('bluebird');
var index = require('./index');

var list = [
    new Promise (function(resolve, reject){
        const event = {
            principalId: 31,
            body: {
                room_id: 40,
                user_id: 31,
                amount: 10
            }
        };
        index.handle(event, {}, function(e, data){
            // console.log('Error', e);
            console.log('Bid Item Answer', data);
            resolve(data);
        })
    })       
];

Promise.all(list).then(function(results){
    console.log(results);
});
