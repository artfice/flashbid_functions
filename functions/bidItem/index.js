'use strict';
var iocContainer = require('./ioContainer');
var lodash = require('lodash');
var Promise = require('bluebird');
var SNSService = require('./service/SNSService');
/*
New Steps
1. Get User ID, Room
2. If current time < flash time -> Exit throw auctionEnd 
3. If user balance is < 25% of nextBidPrice -> Exit say they don't have enough credit to continue
4. If last bidder was them, they can't bid again
SELECT user_id, amount FROM item_bid WHERE item_id = 1 ORDER BY amount DESC LIMIT 1
5. Write to DB the bid
6. Update room
7. call refreshRoom
 */
exports.handle = function (event, context, callback) {
    const userId = event.principalId;
    context.callbackWaitsForEmptyEventLoop = false;
    const id = event.body.room_id;
    const user_id = event.body.user_id;
    const user_amount = event.body.amount;
    let currentRoom = false;
    let currentUser = false;
    let currentBidder = false;
    let nextBidPrice = 0;
    let state = 'open';
    // 1. Get User ID, Room
    iocContainer.db.transaction({
        isolationLevel: iocContainer.isolationLevel
    }).then(function (t) {
        const promiseList = [
            iocContainer.models.users.find({ where: { id: userId } }, { transaction: t }),
            iocContainer.models.rooms.find({ where: { id: id } }, { transaction: t }),
            iocContainer.db.query('SELECT user_id, amount FROM item_bid WHERE room_id = ' + id + ' ORDER BY amount DESC LIMIT 1', { transaction: t })
        ];
        Promise.all(promiseList).then(function (results) {
            currentRoom = lodash.clone(results[1]);
            currentUser = lodash.clone(results[0]);
            const bidHistory = results[2];
            let lastBidder = 0;
            let lastAmount = 0;
            // console.log(bidHistory);
            if (bidHistory && bidHistory[0].length > 0) {
                console.log('BID History', bidHistory[0]);
                lastBidder = bidHistory[0][0].user_id;
                lastAmount = bidHistory[0][0].amount;
            }
            const current_time = new Date().getTime();
            const end_time = currentRoom.end_timestamp;
            const roomAmount = (currentRoom.amount > 0) ? currentRoom.amount : currentRoom.initial_bid;
            nextBidPrice = roomAmount + currentRoom.bid_amount;
            const minBalanceToBid = (nextBidPrice / 4);
           
            // 2. If current time < flash time -> Exit throw auctionEnd 
            if (current_time < currentRoom.flash_timestamp) {
                state = 'notFlashing';
                throw new Error('Room Not Flashing');
            }

            // 3. If user balance is < 25% of nextBidPrice -> Exit say they don't have enough credit to continue
            if (currentUser.balance < minBalanceToBid) {
                state = 'broke';
                throw new Error('User is Broke');
            }

            //4. If current time < flash time -> Exit throw auctionEnd 
            if (end_time <= current_time) {
                state = 'auctionEnded';
                throw new Error('Auction Ended');
            }

            //5. If last bidder was them, they can't bid again
            if (userId == lastBidder) {
                state = 'lastBidder';
                throw new Error('You were the last bidder');
            }

            // 6. Update room
            return currentRoom.updateAttributes({
                amount: nextBidPrice,
                last_bid_name: currentUser.first_name + ' ' + currentUser.last_name.charAt(0)
            }, { transaction: t });

        }).then(function (newRoom) {
            //7. Add Item Bid record
            return iocContainer.models.item_bid.create({
                user_id: userId,
                room_id: currentRoom.id,
                item_id: currentRoom.item_id,
                amount: nextBidPrice
            }, { transaction: t });
        }).then(function (result) {
            //TODO: write to notification
            t.commit();
            const snsService = new SNSService('refreshRoom');
            snsService.deliverMessage({
                id: currentRoom.id
            }).then(function (msg) {
                return callback(null, {
                    statusCode: 200,
                    message: 'Oferta presentada'
                });
            }).catch(function (err) {
                console.log('Send Refresh Room Error SNS', err)
                return callback(null, {
                    statusCode: 200,
                    message: 'Oferta presentada'
                });
            });
        }).catch(function (err) {
            console.log('Bid Room Error ', err);
            t.rollback();
            if (state == 'notFlashing') {
                return callback(null, {
                    statusCode: 403,
                    message: 'La ronda "Flash" no ha comenzado.'
                });
            }
            if (state == 'broke') {
                return callback(null, {
                    statusCode: 403,
                    message: 'Usted no tiene suficiente balance disponible.'
                });
            }
            if (state == 'lastBidder') {
                return callback(null, {
                    statusCode: 403,
                    message: 'Eras el último postor.'
                });
            }

            if (state == 'auctionEnded') {
                const snsService = new SNSService('auctionEnd');
                snsService.deliverMessage({
                    room_id: currentRoom.id
                }).then(function (msg) {
                    return callback(null, {
                        statusCode: 403,
                        message: 'Esta Ronda "Flash" ha concluido',
                    });
                }).catch(function (err) {
                    console.log('Send Auction End Error SNS', err)
                    return callback(null, {
                        statusCode: 403,
                        message: 'Esta Ronda "Flash" ha concluido'
                    });
                });
            }
        });
        //end transaction    
    });
};
