'use strict';
var iocContainer = require('./ioContainer');
var Promise = require('bluebird');
var lodash = require('lodash');
var FirebaseService = require('./service/FirebaseService');
var SNSService = require('./service/SNSService');
var firebaseService = new FirebaseService();
var MailService = require('./service/MailService');
var mailService = new MailService(iocContainer.models);

/*
LotteryOne
7. Write to notification for user
8. Write that they lost in history
9. Send Email
 */
exports.handle = function (event, context, callback) {
    context.callbackWaitsForEmptyEventLoop = false;
    console.log(JSON.stringify(event));
    console.log('Lottery One');
    if (event.Records && event.Records.length > 0) {
        const promiseList = event.Records.map(function (message) {
            let sns = '{}';
            if (message.Sns) {
                sns = message.Sns.Message;
            }
            const payload = JSON.parse(sns);
            const today = new Date();
            const todayStr = today.toISOString().slice(0, 10);

            console.log('Payload', payload);

            let currentUser = null;
            const id = payload.id;
            const winner_id = payload.winner_id;
            const winner_first_name = payload.winner_first_name;
            const title = payload.title;
            const item_image = payload.item_image;

            if (!payload.id || !winner_first_name || !title || !winner_id || !item_image) {
                console.log('Winner or title or id missing', payload);
                return null;
            }
            const notificationMessage = (id != winner_id) ? title + ' ' + 'Perder' : title + ' ' + 'Ganador';
            return iocContainer.models.history.create({
                    user_id: id,
                    message: notificationMessage,
                    read: 0,
                    created_at: todayStr,
                    update_at: today.getTime()
            }).then(function (history) {
                return firebaseService.write('notification', id, {
                    message: notificationMessage
                });
            }).then(function (result) {
                const snsService = new SNSService('sendEmail');
                return snsService.deliverMessage({
                    user_id: id,
                    template: 'winner-lottery',
                    data: {
                        title: title,
                        item_image: item_image,
                        winner_first_name: winner_first_name
                    }
                });                
            }).then(function (result) {              
                const snsService = new SNSService('sendPushNotification');
                return snsService.deliverMessage({
                    user_id: id,
                    title: 'Notificacion',
                    description: notificationMessage
                });                                  
            }).then(function (result) {                
                console.log('Lottery One Done');
                return result;
            }).catch(function (e) {
                console.log('Lottery One ERROR', e);
                return null;
            });
        });

        Promise.all(promiseList).then(function (allRooms) {
            callback(null, allRooms);
        }).catch(function (err) {
            console.log('Lottery One Error', err);
            callback(err);
        });
    }
};