module.exports = function (sequelize, DataTypes) {
  return sequelize.define('bid_cards', {
    code:       {type: DataTypes.STRING, unique: true},
    amount:       DataTypes.INTEGER,
    bonus:       DataTypes.INTEGER,
    user_id:       DataTypes.INTEGER,
    status:       DataTypes.INTEGER,
    created_at: DataTypes.DATE,
    update_at: DataTypes.DATE,
  }, {
        timestamps: false,
        freezeTableName: true,
        tableName: 'bid_cards'
    });
};