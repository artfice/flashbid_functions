module.exports = function (sequelize, DataTypes) {
  return sequelize.define('emails', {
  slug:       {type: DataTypes.STRING, unique: true},
  subject:       DataTypes.STRING,
  html:       DataTypes.TEXT
}, {
        timestamps: false,
        freezeTableName: true,
        tableName: 'emails'
    });
};