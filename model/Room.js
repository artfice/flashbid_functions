module.exports = function (sequelize, DataTypes) {
return sequelize.define('rooms', {
  start_timestamp: DataTypes.INTEGER,
  flash_timestamp: DataTypes.INTEGER,
  end_timestamp: DataTypes.INTEGER,
  item_id: DataTypes.INTEGER,
  item_title: DataTypes.TEXT,
  item_image: DataTypes.TEXT,  
  initial_bid: DataTypes.INTEGER,
  amount: DataTypes.INTEGER,
  winner_id: DataTypes.INTEGER,
  display_photo: DataTypes.STRING,
  bid_history: DataTypes.TEXT,
  state: DataTypes.ENUM('Pending','Open','Flash','Close') 
}, {
    timestamps: false,
    freezeTableName: true,
    tableName: 'rooms'
});
};