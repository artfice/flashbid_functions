module.exports = function (sequelize, DataTypes) {
return sequelize.define('item_user', {
  user_id:       DataTypes.INTEGER,
  item_id:       DataTypes.INTEGER
}, {
    timestamps: false,
    freezeTableName: true,
    tableName: 'item_user'
});
};