module.exports = function (sequelize, DataTypes) {
    return sequelize.define('about', {
        description: DataTypes.TEXT
    }, {
        timestamps: false,
        freezeTableName: true,
        tableName: 'about'
    });
};